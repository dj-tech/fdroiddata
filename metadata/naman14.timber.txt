Categories:Multimedia
License:GPLv3+
Web Site:https://github.com/naman14/Timber/blob/HEAD/README.md
Source Code:https://github.com/naman14/Timber
Issue Tracker:https://github.com/naman14/Timber/issues
Changelog:https://github.com/naman14/Timber/blob/HEAD/Changelog.md

Auto Name:Timber
Summary:Material Design Music Player
Description:
Timber is a Music Player currently in Beta
.

Repo Type:git
Repo:https://github.com/naman14/Timber

Build:0.11b,3
    commit=9ae127610c18e22716ad4d07471e3955fd11f347
    subdir=app
    gradle=yes

Build:0.122b,6
    commit=f95512a84b2876e3afe7101f0cb1ef49ce829c20
    subdir=app
    gradle=yes

Build:0.13b,7
    commit=646f85358541188adb8709190f34b5cdfc35d07f
    subdir=app
    gradle=yes

Build:0.14b,8
    commit=0.14b
    subdir=app
    gradle=yes

Build:0.21b,13
    commit=60271195867980882ee9a091522a2889e587af3e
    subdir=app
    gradle=yes

Build:0.2b,14
    commit=3406f4e932a8c93b865ca6788a21d36be129fbca
    subdir=app
    gradle=yes

Maintainer Notes:
Uses chraslytics now, see https://github.com/naman14/Timber/issues/219. Only build
from f-droid branch: https://github.com/naman14/Timber/commits/f-droid
.

Auto Update Mode:None
Update Check Mode:RepoManifest/f-droid
Current Version:0.2b
Current Version Code:14
